import game.utils as utils
from game.posts import post

class Game:
    def __init__(self, session):
        self.session = session
    def first_post(self):
        return 'Hello. Welcome to Game.'
    def respond(self, cmd):
        raw = cmd
        cmd = utils.clean(cmd)
        if cmd == "":
            return None, False, None, None
        if cmd == 'goodbye':
            self.reset()
            return None, False, None, None
        return raw, f'You said: {raw}', 'You', 'Admin'
    def reset(self):
        self.session.clear()
        self.session['postlist'] = [post(None, self.first_post(), None, 'Admin')]

class Location:
    def __init__(self, x, y, desc, exits):
        self.x = x
        self.y = y
        self.desc = desc
        self.exits = exits
