import string

def clean(s):
    return s.translate(str.maketrans({key: None for key in string.punctuation})).lower().strip()
