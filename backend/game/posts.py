def post(cmd=None, rsp=None, usr=None, pr=None):
    ret = {}
    if cmd:
        ret['command'] = cmd
    if rsp:
        ret['response'] = rsp
    if usr:
        ret['user'] = usr
    if pr:
        ret['peer'] = pr
    return ret
