from flask import (
    Blueprint, flash, g, render_template, request, url_for, session
)
from game.posts import post
from game.logic import Game

game_bp = Blueprint('game', __name__, template_folder='templates')
game = Game(session)

@game_bp.route('/', methods=('GET', 'POST'))
def register():
    if not 'postlist' in session:
        session['postlist'] = [post(None, game.first_post())]
    if request.method == 'POST':
        raw = request.form['prompt']
        cmd, rsp, usr, pr = game.respond(raw)
        if rsp:
            session['postlist'].append(post(cmd, rsp, usr, pr))
            session.modified = True

    return render_template('page.html', post_history=session['postlist'])
